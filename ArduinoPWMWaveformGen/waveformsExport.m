%%
pwmInterval_s = 1/pwmRate_Hz;

armLeadIn_s = 3;
armRampUp_s = 2;
armRampDown_s = 1;

deltaToFirst = data(1) - 950;
deltaSteps = (deltaToFirst / armRampUp_s)/pwmRate_Hz;

armLeadInEndIndex = pwmRate_Hz*armLeadIn_s;
armRampUpStartIndex = armLeadInEndIndex + 1;
armRampUpEndIndex = armLeadInEndIndex + pwmRate_Hz*armRampUp_s;

output = 0;
output(1:armLeadInEndIndex) = 950;
for i = armRampUpStartIndex:armRampUpEndIndex
    output(i) = 950+deltaSteps*(i-armRampUpStartIndex);
end

output = [output data];

armRampDownStartIndex = length(output) + 1;
armRampDownEndIndex = length(output) + pwmRate_Hz*armRampDown_s;

deltaFromEnd = output(end) - 950;
deltaDownSteps = (deltaFromEnd / armRampDown_s)/pwmRate_Hz;
deltaDownStart = output(end);

for i = armRampDownStartIndex:armRampDownEndIndex
    output(i) = deltaDownStart+deltaDownSteps*(armRampDownStartIndex-i);
end

%%
figure();
tttt = 0:pwmInterval_s:((length(output)-1)*pwmInterval_s);
plot(tttt,output,'DisplayName','data');
xlabel('Time (s)');
ylabel('PWM (us)');
title([outName ' PWM Freq: ' num2str(pwmRate_Hz) 'Hz']);
grid on
% legend show

saveas(gcf,[outName '.jpg']);

save([outName '.mat']);


%%
outFileName = [outName '.h'];

f = fopen(outFileName,'w');

fprintf(f,'#ifndef %s_h\n',outName);
fprintf(f,'#define %s_h\n\n',outName);
fprintf(f,'unsigned long %sInterval_us = %i;\n',outName,pwmInterval_s*1000000);
fprintf(f,'const uint16_t  %s[] PROGMEM = {900,\n',outName);

for i = 1:length(output)
    fprintf(f,'%u,\n',floor(output(i)));
end


fprintf(f,'900};\n\n');
fprintf(f,'#endif\n');
fclose(f);