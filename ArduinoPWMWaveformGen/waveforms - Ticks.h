#include <SPI.h>

#ifndef waveforms_h
#define waveforms_h

int waveformIdle[2] = {0, 900};

int waveformInterval_us = 1000000;

// Waveform 1
unsigned long waveform1Interval_us = waveformInterval_us;
const uint16_t  waveform1[] PROGMEM  = {1500,1502};

// WF 2
unsigned long waveform2Interval_us = waveformInterval_us;
const uint16_t  waveform2[] PROGMEM = {1500,1504};

// WF 3
unsigned long waveform3Interval_us = waveformInterval_us;
const uint16_t waveform3[] PROGMEM = {1500,1506};

unsigned long waveform4Interval_us = waveformInterval_us;
const uint16_t  waveform4[] PROGMEM = {1500,1508};

unsigned long waveform5Interval_us = waveformInterval_us;
const uint16_t  waveform5[] PROGMEM = {1500,1510};

unsigned long waveform6Interval_us = waveformInterval_us;
const uint16_t  waveform6[] PROGMEM = {1500,1515};

unsigned long waveform7Interval_us = waveformInterval_us;
const uint16_t  waveform7[] PROGMEM = {1500,1515};

unsigned long waveform8Interval_us = waveformInterval_us;
const uint16_t  waveform8[] PROGMEM = {1500,1516};


                      

                      #endif
