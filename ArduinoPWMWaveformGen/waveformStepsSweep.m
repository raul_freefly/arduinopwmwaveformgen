load('random.mat');

%% Fast Random
outName = 'waveform7';
pwmRate_Hz = 400;
data = random * 450 + 1000;
waveformsExport;

%% Medium Random
outName = 'waveform6';
pwmRate_Hz = 400;

slowDownRatio = 8;
replayRate_Hz = pwmRate_Hz / slowDownRatio;
newData = random(1:end/4) * 450 + 1000;
oldTime = 0:(1/replayRate_Hz):((length(newData)-1)/replayRate_Hz);
newTime = 0:(1/pwmRate_Hz):((length(newData)-1)/replayRate_Hz);

data = interp1(oldTime,newData,newTime);
waveformsExport;

%% Slow Random
outName = 'waveform5';
pwmRate_Hz = 400;

slowDownRatio = 20;
replayRate_Hz = pwmRate_Hz / slowDownRatio;
newData = random(1:end/8) * 450 + 1000;
oldTime = 0:(1/replayRate_Hz):((length(newData)-1)/replayRate_Hz);
newTime = 0:(1/pwmRate_Hz):((length(newData)-1)/replayRate_Hz);

data = interp1(oldTime,newData,newTime);
waveformsExport;

%% Chirp

Fs=400; % Rate
tf=10; % Time
f=5e-6;
t=0:1/Fs:tf-1/Fs;
f1=0.2; % Start Freq
f2=5;   % End Freq
SLOPE=(f2-f1)./t(end);
F=f1+SLOPE*t;
y=-1/2*cos(2*pi*F.*t)+0.5;
plot(t,y)

outName = 'waveform4';
pwmRate_Hz = 400;
data = y*450 + 1000;
waveformsExport;