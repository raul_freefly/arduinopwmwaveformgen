pwmRate_Hz = 400;
pwmInterval_s = 1/pwmRate_Hz;



startTime = t(1);
endTime = t(end);
deltaTime = endTime-startTime;

newTime = 0:pwmInterval_s:deltaTime;

x1int = rot90(interp1(t-startTime,x1,newTime),3);

%%
figure();
plot(t-startTime,x1,'DisplayName','Original');
hold on;
plot(newTime, x1int,'DisplayName','Interpolated');
grid on
legend show

%%
data = x1int;
outName = 'waveform8';
waveformsExport;
