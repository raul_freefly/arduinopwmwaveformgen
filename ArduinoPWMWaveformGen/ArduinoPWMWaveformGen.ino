#include <MemoryFree.h>
#include <pgmStrToRAM.h>

#include <Servo.h>
#include "waveforms.h"

#include "waveform2.h"
#include "waveform3.h"
#include "waveform4.h"
#include "waveform5.h"
#include "waveform6.h"
#include "waveform7.h"
#include "waveform8.h"

#ifdef REFRESH_INTERVAL //if the macro MEDIAN_MAX_SIZE is defined 
#undef REFRESH_INTERVAL //un-define it
#define REFRESH_INTERVAL 2500//redefine it with the new value
#endif 

enum waveform {
  IDLE,
  PYRAMID,
  STEP,
  STEADY
};

Servo esc;
int input = 0;
int command = 0;
int pwmOutput_us = 0;
int pwmCommand_us = 0;
int pwmCommand_us_new = 0;
unsigned long startTime_us = 0;
unsigned long time_us;
int state =  0;
unsigned long lastUpdate_s = 0;
bool outEnabled = 0;
int restartCount = 0;

int maxpwmDelta = 1;

int serialUpdateInterval_ms = 500;

static int enablePinOut = 5;
static int enablePinIn = 7;
static int enablePinIn2 = 8;

bool restartFlag = false;
bool autoRestartEnabled = false;

void setup() {
  esc.attach(2);
  pwmOutput_us = 900;
  pwmCommand_us = 900;
  esc.writeMicroseconds(900); 

  Serial.begin(9600);
  Serial.println("Select waveform");

  pinMode(enablePinOut, OUTPUT);
  pinMode(enablePinIn, INPUT_PULLDOWN);
  pinMode(enablePinIn2, INPUT_PULLDOWN);

  digitalWrite(enablePinOut, 1);
}

void loop() {


  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    command = Serial.parseInt();
    restartCount = 0;

    if (9 == command) {
      if (autoRestartEnabled) {
        autoRestartEnabled = false;
        Serial.println("Autorestart DISABLED");
      } else {
        autoRestartEnabled = true;
        Serial.println("Autorestart ENABLED");
      }
      command = state;
    }

  }

  outEnabled = digitalRead(enablePinIn);

  if (0 == outEnabled) {
    command = 0;
  }

  if (command != state || restartFlag) {
    state = command;
    startTime_us = micros();
    time_us = 0;
    lastUpdate_s = 0;
    Serial.print("Starting Waveform: ");
    Serial.println(state, DEC);

    if(restartFlag){
      restartFlag = false;
      Serial.print("Restart Count:");
      Serial.println(restartCount, DEC);
    }

    
  }

  time_us = micros() - startTime_us;

  switch (state) {
    case 0:
      pwmCommand_us = 900;
      pwmCommand_us_new = 900;
      break;
    case 1:
      pwmCommand_us = getPwmValue(time_us, waveform1Interval_us, waveform1, sizeof(waveform1));
      break;
    case 2:
      pwmCommand_us = getPwmValue(time_us, waveform2Interval_us, waveform2, sizeof(waveform2));
      break;
    case 3:
      pwmCommand_us = getPwmValue(time_us, waveform3Interval_us, waveform3, sizeof(waveform3));
      break;
    case 4:
      pwmCommand_us = getPwmValue(time_us, waveform4Interval_us, waveform4, sizeof(waveform4));
      break;
    case 5:
      pwmCommand_us = getPwmValue(time_us, waveform5Interval_us, waveform5, sizeof(waveform5));
      break;
    case 6:
      pwmCommand_us = getPwmValue(time_us, waveform6Interval_us, waveform6, sizeof(waveform6));
      break;
    case 7:
      pwmCommand_us = getPwmValue(time_us, waveform7Interval_us, waveform7, sizeof(waveform7));
      break;
    case 8:
      pwmCommand_us = getPwmValue(time_us, waveform8Interval_us, waveform8, sizeof(waveform8));
      break;
//    case 1:
//      pwmCommand_us_new = 960;
//      break;
//    case 2:
//      pwmCommand_us_new = 1000;
//      break;
//    case 3:
//      pwmCommand_us_new = 1050;
//      break;
//    case 4:
//      pwmCommand_us_new = 1100;
//      break;
//    case 5:
//      pwmCommand_us_new = 1150;
//      break;
//    case 6:
//      pwmCommand_us_new = 1200;
//      break;
//    case 7:
//      pwmCommand_us_new = 1250;
//      break;
//    case 8:
//      pwmCommand_us_new = 1300;
//      break;

    default:
      command = 0;
      break;
  }

//  if(pwmCommand_us_new > pwmCommand_us){
//    pwmCommand_us += maxpwmDelta;
//  }else if(pwmCommand_us_new < pwmCommand_us){
//    pwmCommand_us -= maxpwmDelta;
//  }

  if (0 == pwmCommand_us) {
    
    if (autoRestartEnabled) {
      restartFlag = true;
      restartCount ++;
    } else {
      command = 0;
      
      pwmCommand_us = 900;
      pwmOutput_us = pwmCommand_us;
    }
  }else{
    pwmOutput_us = pwmCommand_us;
  }

  esc.writeMicroseconds(pwmOutput_us);
  

  //long duration = micros() - startTime;

  if (time_us / (serialUpdateInterval_ms * 1000) > lastUpdate_s) {
    lastUpdate_s += 1;
    Serial.print("Wave: ");
    Serial.print(state, DEC);
    Serial.print(" | Time: ");
    Serial.print(time_us / 1000000, DEC);
    Serial.print(" | pwm: ");
    Serial.println(pwmOutput_us, DEC);
    //sSerial.println(micros() - startTime);

    if (0 == outEnabled) {
      Serial.println(" Output Disabled");
    }
  }

  delayMicroseconds(100);
}

//int getPyramidValue(int time_us, long int interval, int* array) {
//  int desiredIndex = time_us / waveformPyramidInterval_us;
//  if (desiredIndex >= sizeof(waveformPyramid) / sizeof(int)) {
//    return 800;
//  } else {
//    return waveformPyramid[desiredIndex];
//  }
//}

int getPwmValue(unsigned long time_us, unsigned long interval_us, const uint16_t data[], int arraySize) {
  int desiredIndex = (int)(time_us / interval_us);
  //return arraySize / sizeof(int);
  int numEls = arraySize / sizeof(uint16_t);

  if (desiredIndex >= numEls) {
    return 0;
  }
  else
  {
    //return numEls;
    return pgm_read_word_near(data + desiredIndex);
    //return data[desiredIndex];
  }
}
